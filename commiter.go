package main

import (
	"log"
	"os"
)

func Absorb(args *Args) error {
	err := os.Setenv("GIT_EDITOR", "true")
	if err != nil {
		log.Fatalf("failed to set git editor %v", err)
	}

	cleanupFn, recoverFn := CommitChanges(args.Target.Commit)

	if err := rebaseToRef(args.Target.Commit); err != nil {
		if args.NoRecover {
			return err
		}
		rebaseAbort()
		recoverFn()
		cleanupFn()
		return err
	}

	cleanupFn()
	return nil
}
