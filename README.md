# git-absorb

A git command inspired by the mercurial command of the same name described
[here](https://groups.google.com/forum/#!topic/mozilla.dev.version-control).

The gist of this command is to automatically fixup (only for now, squash might
come later) uncommitted (though possibly staged) modifications into the
specified ancestor commit in a working branch with no user interaction.

The common use case or workflow is for e.g. to modify commits in response to
issues raised during a code review, or when you change your mind about the
content of existing commits in your working branch.

The equivalent manual workflow for the above use-cases is to do an interactive
rebase, mark the relevant commits with (m)odify, make changes, then sue `git
add` and `git rebase --continue`, OR use `git commit --fixup` and `git rebase
--interactive` with `autosquash == true`.
