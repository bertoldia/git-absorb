package main

import (
	"errors"
	"os/exec"
	"strings"
)

func gitCmd(args ...string) []string {
	return ExecCmd("git", args...)
}

func EnsureCwdIsGitRepo() {
	res, err := exec.Command("git", "rev-parse", "--show-toplevel").CombinedOutput()
	if err != nil {
		Exit(128, string(res))
	}
}

func CurrentBranch() string {
	//return gitCmd("rev-parse", "--abbrev-ref", "HEAD")[0]
	return gitCmd("name-rev", "--name-only", "HEAD")[0]
}

func Upstream(branch string) string {
	return gitCmd("rev-parse", "--abbrev-ref", branch+"@{upstream}")[0]
}

func MergeBase(branch, upstream string) string {
	return gitCmd("merge-base", upstream, branch)[0]
}

// Return the sha1 of all commits in the current branch.
func CommitsInBranch() []string {
	cb := CurrentBranch()
	us := Upstream(cb)
	mb := MergeBase(us, cb)
	return gitCmd("rev-list", mb+".."+cb)
}

// returns (possibly partially) staged files
func stagedFiles() []string {
	return gitCmd("diff-index", "--cached", "--name-only", "HEAD", "--")
}

// returns all dirty files (staged or otherwise)
func dirtyFiles() []string {
	return gitCmd("diff-index", "--name-only", "HEAD", "--")
}

// List either file with staged changes or, if no changes have been staged, all
// dirty files.
func FilesToAbsorb() ([]string, bool) {
	staged := stagedFiles()
	if len(staged) > 0 {
		return staged, true
	}
	return dirtyFiles(), false
}

func resetHeadSoft() {
	gitCmd("reset", "--soft", "HEAD~1")
}

func resetHead() {
	gitCmd("reset", "HEAD~1")
}

func areChangesStaged() bool {
	_, err := exec.Command("git", "diff-index", "--cached", "--quiet", "HEAD", "--").Output()
	return err != nil
}

func rebaseAbort() {
	gitCmd("rebase", "--abort")
}

func rebaseToRef(sha1 string) error {
	args := []string{"rebase", "-i", "--autosquash", sha1 + "~1"}
	if msg, err := exec.Command("git", args...).CombinedOutput(); err != nil {
		return errors.New(string(msg))
	}
	return nil
}

func stash() {
	gitCmd("stash", "--quiet")
}

func StashPop() {
	gitCmd("stash", "pop", "--quiet")
}

// Expand a ref (in the form of a shortened sha1 or other valid ref spec like
// HEAD) to the full sha1. Also useful for verifying the specified ref is valid.
func expandRef(sha1 string) (string, error) {
	res, err := exec.Command("git", "rev-parse", "--verify",
		strings.TrimRight(sha1, " ")).Output()
	if err != nil {
		return "", errors.New("'" + sha1 + "' is not a valid sha1.")
	}
	if len(res) == 0 {
		return "", nil
	}
	return strings.Split(strings.Trim(string(res), " \n"), "\n")[0], nil
}

func humanCommit(sha1 string) string {
	return gitCmd("log", "--pretty=oneline", "--abbrev-commit", "-1", sha1)[0]
}

func HumanCommits(commits []string) []string {
	var result = make([]string, 0)
	for _, sha1 := range commits {
		result = append(result, humanCommit(sha1))
	}
	return result
}

type CleanupFunc func()
type RecoverFunc func()

func noop() {}

func CommitIsInWorkingSet(sha1 string) bool {
	for _, s := range CommitsInBranch() {
		if sha1 == s {
			return true
		}
	}
	return false
}

// Commit uncommitted changes and return cleanup and undo functions. If any
// changes have been staged, only commit those and stash the remaining changes.
// In this case the cleanup operation is to stash pop and the undo operation is
// a soft reset on head. If none of the outstanding changed have been staged,
// commit them all. In this case the cleanup function is a noop and the undo
// operation is a (regular) reset of head.
func CommitChanges(sha1 string) (CleanupFunc, RecoverFunc) {
	if !areChangesStaged() {
		gitCmd("add", "-u")
	}

	var action = "--fixup"
	gitCmd("commit", action, sha1, "--no-edit")

	if stillDirty := dirtyFiles(); len(stillDirty) != 0 {
		stash()
		return StashPop, resetHeadSoft
	}
	return noop, resetHead
}
