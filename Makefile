.DEFAULT_TARGET: build

build:
	go build

clean:
	go clean

install:
	go install

lint:
	gometalinter -D golint
