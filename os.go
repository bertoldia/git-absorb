package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

func ExitOK() {
	Exit(0, "")
}

func Exit(code int, format string, args ...interface{}) {
	if len(format) > 0 {
		if !strings.HasSuffix(format, "\n") {
			format = format + "\n"
		}
		fmt.Printf(format, args...)
	}
	os.Exit(code)
}

func ExecCmd(cmd string, args ...string) []string {
	res, err := exec.Command(cmd, args...).Output()
	if err != nil {
		log.Fatal("'"+cmd+" "+strings.Join(args, " ")+"' failed. ", err)
	}
	return parseCmdExecResult(res)
}

func parseCmdExecResult(res []byte) []string {
	if len(res) == 0 {
		return make([]string, 0)
	}
	return strings.Split(strings.Trim(string(res), " \n"), "\n")
}
